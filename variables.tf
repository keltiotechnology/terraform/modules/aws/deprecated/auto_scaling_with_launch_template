# variable "aws_security_group_ecs_id" {
#   type = string
#   description = "The ID of AWS security_group associcated with the ECS"
# }

variable "autoscaling_group_name" {
  type        = string
  description = "Name of the Autoscaling group"  
}

variable "min_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
}

variable "max_size" {
  type        = number
  description = "The maximum size of the Auto Scaling Group"
}

variable "desired_capacity" {
  type        = number
  description = "The number of Amazon EC2 instances that should be running in the group"
}

variable "minimum_scaling_step_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
}

variable "maximum_scaling_step_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
}

variable "target_capacity" {
  type        = number
  description = "Target utilization for the capacity provider. A number between 1 and 100."
}

variable "image_id" {
  type        = string
  description = "AMI Image ID e.g. ami-0b440d17bfb7989dc. Please refer to this docs https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html"
}

variable "instance_type" {
  type        = string
  description = "Instance type e.g. t2.micro"  
}


variable "subnet_ids" {
  type        = list(string)
  description = "The subnet IDs for ec2 instances"
}

variable "ecs_cluster_name" {
  type        = string
  description = "Name of ECS cluster"
}

variable "vpc_id" {
  type        = string
  description = "ID of VPC"
}

variable "aws_security_group_ecs_id" {
  type        = string
  description = "ID of security group for ecs"
}

variable "aws_iam_instance_profile_name" {
  type        = string
  description = "Name of the AWS IAM instance profile name"
}

variable "capacity_provider_name" {
  type = string
  description  = "Name of the Capacity provider"
  default = "default_capacity_provider"
}

/* For Cloudwatch */
variable "cloudwatch_full_prefix_name" {
  type = string
}

variable "ecsinstancerole_name" {
  type = string
}
