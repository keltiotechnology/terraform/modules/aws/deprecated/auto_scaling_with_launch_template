resource "aws_autoscaling_group" "default" {
  name                                = var.autoscaling_group_name
  min_size                            = var.min_size
  max_size                            = var.max_size
  desired_capacity                    = var.desired_capacity
  launch_template {
    id                                = aws_launch_template.ecs.id
    version                           = "$Latest"
  }
  vpc_zone_identifier                 = var.subnet_ids
  tag {
    key                               = "AmazonECSManaged"
    value                             = ""
    propagate_at_launch               = true
  }
  protect_from_scale_in               = true
  depends_on                          = [aws_launch_template.ecs]

  timeouts {
    delete                            = "3m"
  }

  provisioner "local-exec" {
    when                              = destroy
    working_dir                       = path.module
    command                           = <<-EOT
    python script/turn_off_protection.py "autoscaling_group_name=${self.name},availability_zone=${tolist(self.availability_zones)[0]}"
    EOT
  }


}

resource "aws_ecs_capacity_provider" "default" {
  name                                = var.capacity_provider_name

  auto_scaling_group_provider {
    auto_scaling_group_arn            = aws_autoscaling_group.default.arn
    managed_termination_protection    = "ENABLED"

    managed_scaling {
      maximum_scaling_step_size       = var.maximum_scaling_step_size
      minimum_scaling_step_size       = var.minimum_scaling_step_size
      status                          = "ENABLED"
      target_capacity                 = var.target_capacity
    }
  }
}

