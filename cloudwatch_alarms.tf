locals {
  ag_full_alarm_name  = "AutoscalingInstancesReachMax"
  cpu_full_alarm_name = "AutoscalingCPUGetfull"
  ram_full_alarm_name = "AutoscalingRAMGetfull"
  overload_alarm_name = "AutoscalingOverloadingReach"
}


resource "aws_cloudwatch_metric_alarm" "ag_full" {
  alarm_name                  = local.ag_full_alarm_name
  comparison_operator         = "GreaterThanOrEqualToThreshold"
  evaluation_periods          = "3"
  metric_name                 = "GroupInServiceInstances"
  namespace                   = "AWS/AutoScaling"
  period                      = "60"
  statistic                   = "Minimum"
  threshold                   = var.max_size
  alarm_description           = "This metric monitors number of running ec2 instance in AG"
  insufficient_data_actions   = []

  dimensions = {
      AutoScalingGroupName = var.autoscaling_group_name
  }
}

resource "aws_cloudwatch_metric_alarm" "ag_cpu_full" {
  alarm_name                  = local.cpu_full_alarm_name
  comparison_operator         = "GreaterThanOrEqualToThreshold"
  evaluation_periods          = "3"
  metric_name                 = "CPUUtilization"
  namespace                   = "AWS/EC2"
  period                      = "60"
  statistic                   = "Average"
  threshold                   = "80"
  alarm_description           = "This metric monitors CPU average utilization of ec2 instances in AG"
  insufficient_data_actions   = []

  dimensions = {
      AutoScalingGroupName = var.autoscaling_group_name
  }
}

resource "aws_cloudwatch_metric_alarm" "ag_ram_full" {
  alarm_name                  = local.ram_full_alarm_name
  comparison_operator         = "LessThanOrEqualToThreshold"
  evaluation_periods          = "3"
  metric_name                 = "mem_available_percent"
  namespace                   = "CWAgent"
  period                      = "60"
  statistic                   = "Average"
  threshold                   = "30"
  alarm_description           = "This metric monitors RAM average utilization of ec2 instances in AG"
  insufficient_data_actions   = []

  dimensions = {
      AutoScalingGroupName = var.autoscaling_group_name
  }
}


locals {
  alarm_rule_with_newlines = <<-EOF
ALARM("${local.ag_full_alarm_name}") AND ALARM("${local.cpu_full_alarm_name}") AND ALARM("${local.ram_full_alarm_name}")
EOF
}


resource "aws_cloudwatch_composite_alarm" "overloading_ag" {
  alarm_description = "Alarm when the autoscaling group gets overloading"
  alarm_name        = local.overload_alarm_name

  alarm_rule = trimspace(replace(local.alarm_rule_with_newlines, "/\n+/", " "))
  
  depends_on = [
    aws_cloudwatch_metric_alarm.ag_full,
    aws_cloudwatch_metric_alarm.ag_cpu_full,
    aws_cloudwatch_metric_alarm.ag_ram_full,
  ]
}