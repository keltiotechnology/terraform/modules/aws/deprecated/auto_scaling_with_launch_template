/* For Cloudwatch */

locals {
  cloudwatch_full_prefix_name         = var.cloudwatch_full_prefix_name
  dmesg                               = "/var/log/dmesg"
  docker                              = "/var/log/docker"
  ecs_agent                           = "/var/log/ecs/ecs-agent.log"
  ecs_init                            = "/var/log/ecs/ecs-init.log"
  audit                               = "/var/log/ecs/audit.log"
  messages                            = "/var/log/messages"
  
}
// ---------------------------------------------------------------------------------- //
/*
 * CloudWatch Logs IAM Policy
 */
resource "aws_iam_policy" "policy" {
  name                  = "ECS-CloudWatchLogs"
  description           = "Allow container instances to use the CloudWatch Logs APIs"

  policy = jsonencode({
    Version             = "2012-10-17"
    Statement = [
      {
        Action = [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams"
        ]
        Effect          = "Allow"
        Resource        = "arn:aws:logs:*:*:*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "cloudwatch_attach_ecsinstancerole" {
  role                  = var.ecsinstancerole_name
  policy_arn            = aws_iam_policy.policy.arn
}
// ---------------------------------------------------------------------------------- //

/* Log groups */
// ---------------------------------------------------------------------------------- //
resource "aws_cloudwatch_log_group" "dmesg" {
  name                                = "${local.cloudwatch_full_prefix_name}${local.dmesg}"
  retention_in_days                   = 30
}
resource "aws_cloudwatch_log_group" "docker" {
  name                                = "${local.cloudwatch_full_prefix_name}${local.docker}"
  retention_in_days                   = 30
}
resource "aws_cloudwatch_log_group" "ecs-agent" {
  name                                = "${local.cloudwatch_full_prefix_name}${local.ecs_agent}"
  retention_in_days                   = 30
}
resource "aws_cloudwatch_log_group" "ecs-init" {
  name                                = "${local.cloudwatch_full_prefix_name}${local.ecs_init}"
  retention_in_days                   = 30
}
resource "aws_cloudwatch_log_group" "audit" {
  name                                = "${local.cloudwatch_full_prefix_name}${local.audit}"
  retention_in_days                   = 30
}
resource "aws_cloudwatch_log_group" "messages" {
  name                                = "${local.cloudwatch_full_prefix_name}${local.messages}"
  retention_in_days                   = 30
}
// ---------------------------------------------------------------------------------- //

data "template_file" "init" {
  template                            = "${path.module}/templates/logs.json"
  vars = {
    cloudwatch_full_prefix_name       = local.cloudwatch_full_prefix_name
    dmesg                             = local.dmesg
    docker                            = local.docker
    ecs_agent                         = local.ecs_agent
    ecs_init                          = local.ecs_init
    audit                             = local.audit
    messages                          = local.messages
  }
}

locals {
  cloudwatch_config = templatefile("${path.module}/templates/logs.json", {
    cloudwatch_full_prefix_name       = local.cloudwatch_full_prefix_name
    dmesg                             = local.dmesg
    ecs_agent                         = local.ecs_agent
    ecs_init                          = local.ecs_init
    audit                             = local.audit
    messages                          = local.messages
    autoscaling_group_name            = "\\$${aws:AutoScalingGroupName}"
    instanceId                        = "\\$${aws:InstanceId}"
  })

  cmd                                 = <<EOT
#!/bin/bash
VALUE=$(cat <<EOF
${local.cloudwatch_config}
EOF
)

(echo $VALUE) | sudo sh -c 'tee -a > config.json'

yum install -y amazon-cloudwatch-agent

sudo mv config.json /opt/aws/amazon-cloudwatch-agent/bin/

yum install -y jq

sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/bin/config.json

echo ECS_CLUSTER=${var.ecs_cluster_name} >> /etc/ecs/ecs.config
EOT
}


# !Image ID depends on region
resource "aws_launch_template" "ecs" {
  name_prefix                         = "launch_template_ecs"
  image_id                            = var.image_id
  instance_type                       = var.instance_type

  network_interfaces {
    associate_public_ip_address       = false
    security_groups                   = [var.aws_security_group_ecs_id]
  }

  iam_instance_profile {
    name                              = var.aws_iam_instance_profile_name
  }
  user_data                           = base64encode(local.cmd)
}
